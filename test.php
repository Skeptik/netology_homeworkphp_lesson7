<?php
require_once __DIR__.'/function.php';
if (empty($_GET['nameTest'])){
  $protocol = $_SERVER['SERVER_PROTOCOL'];
  header("$protocol 404 Not Found");
	die;
}else{
	$nameTest = $_GET['nameTest'];
	if(file_exists(__DIR__."/$nameTest")){
		$arrTest= json_decode(file_get_contents(__DIR__."/$nameTest"), true);
	}else{
		echo "<center><h1>Файл не найден.</h1></center>";
		back();
		die;
	}
}
menu($nameTest);
?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Выполнение теста</title>
 	<style type="text/css">
   div { 
    padding: 7px;
    padding-right: 20px; 
    border: solid 1px black;
    font-family: Verdana, Arial, Helvetica, sans-serif; 
    font-size: 13pt; 
   	background: #E6E6FA;
   }
   body{
   	background: #159445;
   }
  </style> 
 </head>
 <body>
 	<h1 align="center"><?= $_GET['nameTest'] ?></h1>
 	<form method="POST">
    <label for="firstName"><strong style="font-size: 20pt">Ваше имя: </strong></label><input type="text" name="firstName">
 	<?php
 	foreach ($arrTest as $key => $value) {
 		echo "<div>";
 		foreach ($value as $key1 => $value1) {
 			if(!is_array($value1) && $key1!='corrected'){
 				echo '<strong>'.$value1.'</strong>'.'<br>';
 			}
 			if(is_array($value1)){
 				foreach ($value1 as $key2 => $value2) {
 					echo '<label><input value='.$key2.' type="radio" name='.$key1.'>'.$value2.'</label>';
 				}	
 			}
 		}
 		echo "</div>";
 	}
 	?>
 	<input name="but" type="submit" value="Выполнить тест">
 	</form>
 </body>
 </html>

 <?php 
 $results = 0;
 if($_POST){
  if ($_POST['firstName']){
    $userName = $_POST['firstName'];
    if(count($_POST)-2 == count($arrTest)){
    foreach ($arrTest as $k => $v) {
      foreach ($v as $k1 => $v2) {
        if (is_array($v2) && $_POST[$k1] == $arrTest[$k]['corrected']){
          $results++;
        }
      }
    }
    $length = count($arrTest);
    echo "<center><img src='image.php?results=$results&length=$length&userName=$userName'></center><br>";
    }else{
      echo '<div align="center"><h1 style="color: red">Выбраны не все ответы.</h1></div>';
    }
  }else{
    echo '<div align="center"><h1 style="color: red">Вы не ввели имя.</h1></div>';
  }
 }
 ?>
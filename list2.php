<?php
require_once __DIR__.'/function.php';
if(!empty($_GET['fileName'])){
	$fileName = $_GET['fileName'];
}else{
	echo "<center><h1>Файл не был загружен</h1></center>";
	back();
	die;
}
menu($fileName);
?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 	<style type="text/css">
   		div { 
    	  padding: 7px;
    	  padding-right: 20px; 
    	  border: solid 1px black;
   		  font-family: Verdana, Arial, Helvetica, sans-serif; 
   	 	  font-size: 13pt; 
   		  background: #E6E6FA;
  		}
</style>
 </head>
 <body>
 	<div align="center">
 	<ul>
 			<h2>Выполнить тест: <a href="test.php?nameTest=<?= $fileName ?>"><?= $fileName ?></a></h2>	
 	</ul>
 	</div>
 </body>
 </html>
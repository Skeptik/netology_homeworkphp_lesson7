<?php
require_once __DIR__.'/function.php';
if(!empty($_FILES['input']['name'])){
	$fileName = $_FILES['input']['name'];
	$explode = explode('.', $fileName);
	if($explode[1] != 'json'){
		echo "<center><h1>Можно загружать только файл с расширением json.</h1></center>";
		back();
		die;
	}
	$arrTest = json_decode(file_get_contents($_FILES['input']['tmp_name']));
	if(!validJson($arrTest)){
		echo "<center><h1>Не верная структура JSON файла.</h1><center>";
		back();
		die;
	}
	if (!move_uploaded_file($_FILES['input']['tmp_name'], $fileName) && !$_FILES['input']['error'] == UPLOAD_ERR_OK){
		echo "<center><h1>Не удалось загрузить файл с тестом.</h1></center>";
		back();
		die;
	}
}else{
	echo "<center><h1>Файл не был загружен</h1></center>";
	back();
	die;
}
redirect($fileName);
?>
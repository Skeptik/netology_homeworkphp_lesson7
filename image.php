<?php 
$path = __DIR__.'/png/pumpkin.png';
 $fontFile = __DIR__.'/font/Alex.ttf';
 if(!file_exists($path)){
 	echo "<h1>Картинка не найдена.</h1>";
 	exit();
 }
 if(!file_exists($fontFile)){
 	echo "<h1>Файл с шрифтом не найден.</h1>";
 	exit();
 }
 $image = imagecreatetruecolor(900, 250);
 $backColor = imagecolorallocate($image, 7, 245, 15);
 $textColor = imagecolorallocate($image, 61, 11, 77);
 $myImage = imagecreatefrompng($path);
 $text = $_GET['userName'].' вы ответили';
 $textResult = ' на '. $_GET['results'].' из '.$_GET['length'].' вопросов.';
 imagefill($image, 10, 10, $backColor);
 imagecopy($image, $myImage, 630, 0, 0, 0, 280, 282);
 imagettftext($image, 80, 0, 40, 90, $textColor, $fontFile, $text);
 imagettftext($image, 80, 0, 20, 180, $textColor, $fontFile, $textResult);
 header('Content-Type: image/png');
 imagepng($image);
 imagedestroy($image);
 ?>